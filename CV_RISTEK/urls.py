"""CV_RISTEK URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.views.generic.base import RedirectView
from CV.views import home, about, resume, contact, show_message, add_message

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home', home, name='home'),
    path('about', about, name='about'),
    path('resume', resume, name='resume'),
    path('contact-me', contact, name='contact'),
    path('message', show_message, name='message'),
    path('send-message', add_message, name='send-message'),
    path('', RedirectView.as_view(url='home', permanent=True)),
]
