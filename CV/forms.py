from django import forms

class MessageForm(forms.Form):
    name = forms.CharField(label='', max_length=27, required=True,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}))
    message = forms.CharField(label='', max_length=300, required=True,
                               widget=forms.Textarea(attrs={'class': 'form-control', 
                               'placeholder': 'Message', 
                               'rows': 6, 
                               'cols': 22, 
                               'style': 'resize:none;'}))
                               
