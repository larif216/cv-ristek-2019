from django.db import models

# Create your models here.
class Message(models.Model):
    name = models.CharField(max_length=27)
    date = models.DateTimeField(auto_now_add=True)
    message = models.CharField(max_length=300)

    class Meta:
        verbose_name_plural = 'Message'

    def __str__(self):
        return self.name