from django.shortcuts import render
from django.http import HttpResponseRedirect
from datetime import date
from .forms import MessageForm
from .models import Message

# Create your views here.
def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

response = {
    'author': 'Muhamad Lutfi Arif',
    'headline': 'Your Future Programmer',
    'birthdate': 'October 23, 1999',
    'age': calculate_age(date(1999, 10, 23)),
    'phone': '+62 813-1754-1282',
    'email': 'Larif216@gmail.com',
    'location': 'Central Jakarta, DKI Jakarta, Indonesia',
    'quote': 'The best of you are those who benefits others'
}
def home(request):
    return render(request, 'home.html', response)

def about(request):
    return render(request, 'about.html', response)

def resume(request):
    return render(request, 'resume.html', response)

def contact(request):
    return render(request, 'contact.html', response)

def show_message(request):
    messages = Message.objects.all()
    response['form'] = MessageForm
    response['messages'] = messages
    return render(request, 'message.html', response)

def add_message(request):
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            name = request.POST['name']
            message = request.POST['message']
            p = Message(name = name, message = message)
            p.save()
            return HttpResponseRedirect('/message')
    else: 
        form = MessageForm()
    return render(request, 'message.html', {'form': form})