Link : http://muhamad-lutfi-arif.herokuapp.com/

Pentujuk untuk menjalankan di localhost
1. Buat sebuah virtual environment dengan menggunakan commad `python -m venv env` di cmd windows
2. Aktifkan virtual environemt dengan command `env\Scripts\activate.bat`
3. Masuk ke folder `CV RISTEK`
4. Install semua requirement modul dengan commad `pip install -r "requirements.txt"`
5. Jalankan perintah `python manage.py runserver`
6. Buka browser dan masukkan `http://127.0.0.1:8000` kolom alamat